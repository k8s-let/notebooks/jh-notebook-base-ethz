FROM jupyter/datascience-notebook:hub-3.0.0

USER root

# Install ETHZ root ca certificates
ADD http://pkiaia.ethz.ch/aia/ETHZRootCA2020.pem /usr/local/share/ca-certificates/ETHZRootCA2020.crt
ADD http://pkiaia.ethz.ch/aia/ETHZIssuingCA2020.pem /usr/local/share/ca-certificates/ETHZIssuingCA2020.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalRootCA.pem /usr/local/share/ca-certificates/DigiCertGlobalRootCA.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertTLSRSASHA2562020CA1-1.pem /usr/local/share/ca-certificates/DigiCertTLSRSASHA2562020CA1-1.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalRootG2.pem /usr/local/share/ca-certificates/DigiCertGlobalRootG2.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalG2TLSRSASHA2562020CA1.pem /usr/local/share/ca-certificates/DigiCertGlobalG2TLSRSASHA2562020CA1.crt
RUN chmod 644 /usr/local/share/ca-certificates/* && update-ca-certificates

# OS update, basic addon commands, and Owncloud client (polybox)
RUN apt-get update && apt-get upgrade -y && \
  apt-get install -y \
    acl \
    cmake \
    gpg \
    less \
    libgl1-mesa-glx \
    libglu1-mesa \
    libxft2 \
    libxdamage-dev \
    lmodern \
    man \
    python3-pip \
    povray \
    texlive-xetex \
    texlive-lang-german \
    texlive-science \
    tesseract-ocr \
    tesseract-ocr-eng \
    tesseract-ocr-deu \
    vim \
    xvfb \
    zip \
    && \
  wget -nv https://download.owncloud.com/desktop/ownCloud/stable/latest/linux/Ubuntu_22.04/Release.key -O - | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/owncloud.gpg > /dev/null && \
  echo 'deb https://download.owncloud.com/desktop/ownCloud/stable/latest/linux/Ubuntu_22.04/ /' | sudo tee -a /etc/apt/sources.list.d/owncloud.list && \
  apt-get update && \
  apt-get install -y owncloud-client && \
  apt-get clean

RUN mamba update --all \
  && \
  mamba install nbgitpuller webcolors uri-template isoduration fqdn jupyterlab-git \
  && \
  mamba install -c python-control control \
  && \
  mamba clean --all

# Seems to be unsupported in Jupyter images used here
# RUN jupyter contrib nbextension install && jupyter nbextension enable toc2/main

RUN jupyter labextension disable "@jupyterlab/apputils-extension:announcements"

# Installed with pip, causes issues with mamba installs
RUN pip3 uninstall  -y traitlets

# Fix timezone
RUN ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime && echo "Europe/Zurich" >> /etc/timezone

USER 1000
